let Bot = class {
    constructor(id, user, profil){
        this.id = id;
        this.user = user;
        this.profil = profil;
    }

    getInfo(){
        return {
            id:this.id,
            user:this.user,
            profil:this.profil
        }
    }
    getId(){
        return this.id;
    }
    getUser(){
        return this.user;
    }
    getProfil(){
        return this.profil;
    }

    setId(id){
        this.id = id;
    }
    setUser(user){
        this.user = user;
    }
    setProfil(profil){
        this.profil = profil;
    }
}

module.exports = Bot;