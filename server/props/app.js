const express =  require('express')
const app = express();
const http = require('http');
const server = http.createServer(app);
const { Server } = require("socket.io");
const io = new Server(server);

const current_datetime = require('./current_date.js');
const commands_request = require('./commands.js');
const Bot = require('./bots_objects.js');

let bots = [];

const newBots = () => {
    let b0 = new Bot(0,'YourAge','https://cdn.pixabay.com/photo/2017/12/31/15/56/portrait-3052641_960_720.jpg');
    let b1 = new Bot(1,'Joker','https://cdn.pixabay.com/photo/2020/08/13/16/25/joker-5485787_960_720.jpg');
    let b2 = new Bot(2,'Doggo','https://cdn.pixabay.com/photo/2016/11/19/15/20/dog-1839808_960_720.jpg');
    let b3 = new Bot(3,'Helper','https://cdn.pixabay.com/photo/2016/01/19/07/35/strength-1148029_960_720.jpg');
    bots.push(b0);
    bots.push(b1);
    bots.push(b2);
    bots.push(b3);
}

app.use(express.static('public'));
app.use('/css',express.static(`${process.cwd()}/public/css`));
app.use('/js',express.static(`${process.cwd()}/public/js`));

app.get('/', (req, res) => {
    res.sendFile(`${process.cwd()}/views/index.html`);    
});

app.get('/bots', (req, res) => {
    let b = []
    bots.forEach((bx)=>{
        b.push(bx.getInfo());
    });
    res.json({bots:b})
});

io.on('connection', (socket) => {
    console.log('a user connected');
    socket.on('disconnect', () => {
        console.log('user disconnected');
    });

    socket.on('chat',async (m) => {

        m['datetime'] = current_datetime();
        io.emit('chat', m);

        let command = await commands_request(m['user'],m['message'],bots);

        for(const x of command){
            x['datetime'] = current_datetime();
            io.emit('chat', x);
        }
        
    });
});
  
server.listen(5500, () => {
    console.log('listening on port : 5500');
});

newBots();