const api = require('./apis.js');
const api_age = api.api_age;
const api_joke = api.api_joke;
const api_dog = api.api_dog;

const commands = {
    'hello':{
        res: 'hello',
        for: false,
        des: 'to say hello (@)',
    },
    'yo':{
        res: 'hey',
        for: false,
        des: 'to say yo (@)',
    },
    'welcome':{
        to:'hello',
    },
    'hi':{
        to:'yo',
    },

    'age':{
        res: api_age,
        for: [0],
        api: true,
        des: 'find an age from a name : <b>--age-{<i>name</i>}</b>',
    },
    'joke':{
        res: api_joke,
        for: [1],
        api: true,
        des: 'a funny little joke',
    },
    'dog':{
        res: api_dog,
        for: [2],
        api: true,
        des: 'send a dog picture',
    },

    'ping':{
        res: 'pong',
        for: false,
        des: 'pong ! (@)'
    },

    'help':{
        res: '',
        help: true,
        for: [3],
        des: 'understand the commands',
    },
    'h':{
        to: 'help',
    }
}

let get_tag = (m) => {
    let t = [];
    m.split(' ').forEach((w)=>{
        if(w.charAt(0)=='@'){
            t.push(w.replace(/^./, ''));
        }
    });
    return t;
}

let match_tag = (t,b) => {
    let f = {};
    t.forEach((tx)=>{        
        b.forEach((bx)=>{
            if(tx.toLowerCase()==bx.getUser().toLowerCase()){
                f[bx.getUser()] = bx.getInfo();
            }
        });
        if(tx=='all'){
            b.forEach((bx)=>{
                f[bx.getUser()] = bx.getInfo();                
            });
        }
    });
    return f;
}

let match_id = (i,b) => {
    let f = {};
    i.forEach((ix)=>{
        b.forEach((bx)=>{
            if(ix==bx.getId()){
                f[bx.getUser()] = bx.getInfo();
            }
        });
    });
    return f;
}

let get_command = (m) => {
    let c = [];
    m.split(' ').forEach((w)=>{
        if(w.charAt(0)=='-'&&w.charAt(1)=='-'){
            c.push(w.replace(/^./, '').replace(/^./, '').split('-')[0]);
        }
    });
    return c;
}

let response_command = (r,match) => {
    let messages = [];
    for (const [key, value] of Object.entries(match)) {
        let m = {
            message: r,
            user: key,
            profil: value.profil,
            bot: true,
        }
        messages.push(m);
    }
    return messages;
}

let help = () => {
    let h = '';
    for (const [key, value] of Object.entries(commands)) {
        
        let x = key;
        let v = value
        if('to' in value){
            v = commands[commands[x].to];
        } 
        
        h += `<b>--${x}</b> ${v.des}\n`;

    }
    h += '\n<b style="color:orange;">/!\\</b> You must specify the name of a bot or "@all" for commands with "@". ex: "<b><i>@Helper</i> --hello</b>". <b style="color:orange;">/!\\</b>';
    h += '\n\nClick on a bot in the contact list (to the left) to @ it.'
    return h;
}

let commands_request = async (user, message, bots) => {
    let tags = await get_tag(message);
    let messages = [];
    
    let cs = await get_command(message)
   
    for(const c of cs){
        if(c in commands){
            let x = commands[c];
            let r = '';

            if('to' in x){
                x = commands[x.to];
            }

            if('api' in x){
                r = await x.res(message);
            }else if('help' in x){
                r = help();
            }else{
                r = x.res;
            }



            if(!x.for){ // all bots (can @)                
                messages.push.apply(messages,response_command(r,match_tag(tags,bots)));                
            }else{ // bots by ids                             
                messages.push.apply(messages,response_command(r,match_id(x.for,bots)));                   
            }
        }
    };

    if(messages.length==0){
        messages.push.apply(messages,response_command('use <b>--help</b> for find commands',match_tag(tags,bots))); 
    }
  
    return await messages;
}

module.exports = commands_request;