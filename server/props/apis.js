const https = require('https');

let htr = (url,json=true) => {
    return new Promise(function (resolve, reject) {
        https.get(url, function(res){
            let body = '';  

            res.on('data', function(chunk){
                body += chunk;
            });    

            res.on('end', () => {
                r = '';
                if(json){
                    r = JSON.parse(body);  
                }else{
                    r = body;
                }                
                resolve(r)
            });

        }).on('error', (e) => {
            reject(e);
        });
    });
}

let api_age = async (message) => {   
    let name = 'sylvain';

    message.split(' ').forEach((w)=>{
        if(w.includes('--age-')){
            n = w.split('--age-')[1];
            if(n!=''){
                name = n.toLowerCase();
            }
        }
    });

    let get_age = `https://api.agify.io/?name=${name}`; 
       
    let r = await htr(get_age);

    name = name.charAt(0).toUpperCase() + name.slice(1);

    return `The average age for the name <b>${name}</b> is <b>${r.age}</b> years old !`;    
}

let api_joke = async (message) => {
    let get_joke = `https://v2.jokeapi.dev/joke/Any?type=single`;

    let r = await htr(get_joke);

    return r.joke;
}

let api_dog = async (message) => {
    let get_ip = `https://dog.ceo/api/breeds/image/random`;

    let r = await htr(get_ip);

    return `<img class="message__image" src="${r.message}">`;
}

module.exports = {
    api_age,
    api_joke,
    api_dog,
};