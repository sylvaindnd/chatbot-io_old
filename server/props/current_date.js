let get_datetime = () => {
    let currentdate = new Date(); 
    return {
        'date':[currentdate.getDate(),(currentdate.getMonth()+1),currentdate.getFullYear()],
        'time':[currentdate.getHours(),currentdate.getMinutes(),currentdate.getSeconds()]
    } 
}

module.exports = get_datetime;