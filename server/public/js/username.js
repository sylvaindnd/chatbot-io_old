let USERNAME = '';

export function init(){
    event();
}

function event(){
    $('#username .username__input_container button').click(function(){
        let u = $(this).parent().find('input#username__input').val();
        if(u.length<3){
            u = 'Username' + randomInt(1000, 9999);
        }
        USERNAME = u;
        $('#username').remove();
        $('#username .username__input_container button').off();
    });
}

function randomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}
  

export function getUsername(){
    return USERNAME;
}