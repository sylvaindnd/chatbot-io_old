export let Bot = class{
    constructor(){
        this.getBots();
    }

    event(){
        $('#contacts .contact').click(function(){
            let user = '@'+$(this).find('span.contact_pseudo').text();
            let i = $('#new_message input');
            i.val(i.val() +' '+ user);
            $('#new_message input').focus();
        });
    }
    
    getBots(){
        let self = this;
        $.getJSON('bots', function(data) {
            let bots = data.bots;
            self.appendBots(bots);
        });
    }
    
    appendBots(bots){
        let self = this;
        bots.forEach((b)=>{
            self.addBots(b);
        });
        self.event();
    }
    
    addBots(b){
        let html = `
        <div class="contact d-flex align-items-center rounded text-light p-2 mb-2 position-relative isbot" data-contactid="${b.id}">
            <img src="${b.profil}" class="rounded-circle" alt="...">
            <span class="contact_pseudo">${b.user}</span>
            <span class="contact_bot rounded py-1 px-2 fst-italic text-light">bot</span>
        </div>`;
        $('#contacts').append(html);
    }
}
