import * as username from './username.js';
const socket = io();

export let Chat = class {

    constructor(){
        this.unstoreMessage();
        this.event();
    }
    
    event(){        
        let self = this;
        $('#new_message').submit(function(){
            let i = $(this).find('input')
            let u = username.getUsername();
            let m = i.val();
            self.sendMessage(u,m,i);

            self.easteregg(m);

            return false;
        });

        socket.on('chat', function(m) {        
            self.newMessage(m);
        });
    }

    easteregg(m){
        if(m.includes('--dinnerbone')){
            $('body').css('transform','rotate(180deg)');
        }
        if(m.includes('--jeff')){
            let hue = 0
            setInterval(()=>{
                hue++;
                if(hue>360){
                    hue=0;
                }
                $('body').css('filter','hue-rotate('+hue+'deg)');
            },10);
            
        }
    }

    newMessage(m){
        let self = this;

        let u = m.user;
        let d = `${m.datetime.date.join('/')}, ${m.datetime.time.join(':')}`;
        let c = m.message.replace(/\n/g,'<br>');
        let p = m.profil;

        if(!p||p==undefined){
            p = 'https://www.e-xpertsolutions.com/wp-content/plugins/all-in-one-seo-pack/images/default-user-image.png'
        }

        let o = 'message_other';

        if(u==username.getUsername()){
            o = 'message_me';
        }

        self.storeMessage({
            u:u,
            d:d,
            c:c,
            p:p,
            o:o,
        });

        self.renderMessage(u,d,c,p,o);
    }
        

    renderMessage(u,d,c,p,o){
        let h = 
        `<div class="message ${o} d-flex mt-3">
            <div class="d-flex py-3">                    
                <div class="message_image mx-3">
                    <img src="${p}" class="rounded-circle" alt="...">
                </div>
                <div class="message_container">
                    <div class="message_container__pseudo">
                        <span class="message_pseudo__pseudo text-light fw-bold">${u}</span>
                        <span class="message_pseudo__date">${d}</span>
                    </div>
                    <div class="message_container__message">
                        <span>${c}</span>
                    </div>
                </div>
            </div>                
        </div>`;
              
        $('#messenger .messages').append(h);
        $('#messenger .messages .nomessage').remove();  
        $('#messenger .messages').scrollTop($('#messenger .messages')[0].scrollHeight);    

    }

    storeMessage(m){
        let messages = localStorage.getItem('message');

        if(messages==null){
            messages = [];
        }else{
            messages = JSON.parse(messages);
        }

        messages.push(m);

        let str = JSON.stringify(messages);

        localStorage.setItem('message', str);
    }

    unstoreMessage(){
        let self = this;
        let messages = localStorage.getItem('message');

        if(messages==null){
            messages = [];
        }else{
            messages = JSON.parse(messages);
        }

        messages.forEach((m)=>{
            self.renderMessage(m.u,m.d,m.c,m.p,m.o);
        });
    }

    sendMessage(u,m,i){
        if(m==''||m==undefined||!m){
            return false;
        }
        let message = {
            'message':m,
            'user':u
        }
        i.val('');
        socket.emit('chat', message);
    }

}